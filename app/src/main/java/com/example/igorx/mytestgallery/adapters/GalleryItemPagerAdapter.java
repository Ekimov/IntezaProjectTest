package com.example.igorx.mytestgallery.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.igorx.mytestgallery.ImageLab;
import com.example.igorx.mytestgallery.fragments.GalleryItemPagerFragment;


public class GalleryItemPagerAdapter extends FragmentPagerAdapter {

    public GalleryItemPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return GalleryItemPagerFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return ImageLab.getInstance().getImageList().size();
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return ImageLab.getInstance().getImageList().get(position).getName();
    }
}
