package com.example.igorx.mytestgallery.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.igorx.mytestgallery.ImageLab;
import com.example.igorx.mytestgallery.R;
import com.example.igorx.mytestgallery.adapters.GalleryImageAdapter;

import java.io.File;
import java.util.LinkedList;

public class GridGalleryFragment extends Fragment {


    private GridView gvGallery;
    private GalleryImageAdapter adapter;
//    private GalleryShowListener galleryShowListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        galleryShowListener= (GalleryShowListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.grid_gallery_fragment, container, false);
        ImageLab.getInstance().setImageList(findFiles(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)));
        gvGallery=(GridView)view.findViewById(R.id.gvGallery);
        adapter = new GalleryImageAdapter();
        gvGallery.setAdapter(adapter);
        gvGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (isAdded()) {
                    galleryShow(position);
                }
            }
        });
        gvGallery.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                deleteDialog(position);
                return true;
            }
        });
        return view;
    }
    public void galleryShow(int position) {
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.translate_right_in, R.anim.translate_left_out);
        ft.replace(R.id.fragContain, GalleryPagerFragment.newInstance(position));
        ft.addToBackStack(GridGalleryFragment.class.getCanonicalName());
        ft.commit();
    }

    private LinkedList<File> findFiles(File rootDirectory) {
        LinkedList<File> findFileList = new LinkedList<>();
        File[] files = rootDirectory.listFiles();
        for (File singleFile : files) {
            if (singleFile.isDirectory() && !singleFile.isHidden()) {
                findFileList.addAll(findFiles(singleFile));
            } else {
                if ((singleFile.getName().endsWith(".jpg")) || (singleFile.getName().endsWith(".png"))) {
                    findFileList.add(singleFile);
                }
            }
        }
        return findFileList;
    }

    private void deleteDialog(final int position) {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(getActivity());
        alertDialog.setMessage(getResources().getString(R.string.delete_image));
        alertDialog.setPositiveButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.setNegativeButton(getResources().getString(R.string.delete_image),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        boolean isDelImage = ImageLab.getInstance().getImageList().get(position).delete();
                        if (isDelImage) {
                            ImageLab.getInstance().getImageList().remove(position);
                            adapter.notifyDataSetChanged();
                        }
                    }
                });

        alertDialog.show();
    }
}
