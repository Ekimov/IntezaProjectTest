package com.example.igorx.mytestgallery;


import com.squareup.otto.Bus;

public class BusProvider {

    public static final Bus BUS = new Bus();

    private BusProvider() {

    }

    public static Bus getInstance() {
        return BUS;
    }
}
