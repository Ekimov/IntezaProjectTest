package com.example.igorx.mytestgallery;

import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.example.igorx.mytestgallery.fragments.GalleryPagerFragment;
import com.example.igorx.mytestgallery.fragments.GridGalleryFragment;
import com.example.igorx.mytestgallery.interfaces.SendImageEmailInterface;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity implements  GalleryPagerFragment.FabListener {

    private FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragContain);
        if(fragment==null) {
            showImageGridFragment();
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BusProvider.getInstance().post(new SendImageEvent());
            }
        });
    }

    private void showImageGridFragment() {
        GridGalleryFragment gridFragment = new GridGalleryFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragContain, gridFragment);
        ft.commit();
    }

    private LinkedList<File> findFiles(File rootDirectory) {
        LinkedList<File> findFileList = new LinkedList<>();
        File[] files = rootDirectory.listFiles();
        for (File singleFile : files) {
            if (singleFile.isDirectory() && !singleFile.isHidden()) {
                findFileList.addAll(findFiles(singleFile));
            } else {
                if ((singleFile.getName().endsWith(".jpg")) || (singleFile.getName().endsWith(".png"))) {
                    findFileList.add(singleFile);
                }
            }
        }
        return findFileList;
    }

    @Override
    public void showFab() {
        fab.show();
    }

    @Override
    public void hideFab() {
        fab.hide();
    }

}
