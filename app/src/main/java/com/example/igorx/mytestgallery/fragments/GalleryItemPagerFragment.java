package com.example.igorx.mytestgallery.fragments;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.igorx.mytestgallery.BusProvider;
import com.example.igorx.mytestgallery.ImageLab;
import com.example.igorx.mytestgallery.R;
import com.example.igorx.mytestgallery.SendImageEvent;
import com.example.igorx.mytestgallery.adapters.GalleryItemPagerAdapter;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.io.File;

public class GalleryItemPagerFragment extends Fragment {

    private static final String SELECT_POSITION= "POSITION";
    private int mPosition;
    private ImageView ivItemGallery;
    private File imageFile;
    private boolean isVisibleFragment;

    public static Fragment newInstance(int position) {
        Fragment fragment = new GalleryItemPagerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(SELECT_POSITION, position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mPosition=getArguments().getInt(SELECT_POSITION);
        imageFile= ImageLab.getInstance().getImageList().get(mPosition);
        View view = inflater.inflate(R.layout.image_item_layout, container, false);
        ivItemGallery=(ImageView)view.findViewById(R.id.ivItem);
        Picasso.with(getActivity()).load(imageFile).into(ivItemGallery);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void onSendImage(SendImageEvent sendImageEvent) {
        if(isVisibleFragment) {
            try {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType(getMimeType(imageFile.getPath()));
                emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile));
                startActivity(emailIntent);
            } catch (ActivityNotFoundException ex) {
                Toast.makeText(getActivity(), "Ошибка выполнения операции", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisibleFragment=isVisibleToUser;
    }

    public  String getMimeType(String path) {
            String type = null;
            String extension = MimeTypeMap.getFileExtensionFromUrl(path);
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
            return type;
        }

}
