package com.example.igorx.mytestgallery.interfaces;

/**
 * Created by IgorX on 08.05.2016.
 */
public interface SendImageEmailInterface {
    void sendImage(int position);
}
