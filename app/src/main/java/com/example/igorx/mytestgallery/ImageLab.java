package com.example.igorx.mytestgallery;

import java.io.File;
import java.util.List;


public class ImageLab {
    private static ImageLab imageLab;
    private List<File> imageList;
    public static ImageLab getInstance() {
        if (imageLab == null) {
            imageLab = new ImageLab();
        }
        return imageLab;
    }

    private ImageLab() {

    }

    public void setImageList(List<File> imageList) {
        this.imageList = imageList;
    }

    public List<File> getImageList() {
        return imageList;
    }
}
