package com.example.igorx.mytestgallery.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.igorx.mytestgallery.ImageLab;
import com.example.igorx.mytestgallery.R;
import com.squareup.picasso.Picasso;

import java.io.File;

/**
 * Created by IgorX on 07.05.2016.
 */
public class GalleryImageAdapter extends BaseAdapter {

    public GalleryImageAdapter() {

    }

    @Override
    public int getCount() {
        return ImageLab.getInstance().getImageList().size();
    }

    @Override
    public Object getItem(int position) {
        return ImageLab.getInstance().getImageList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        File file = ImageLab.getInstance().getImageList().get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery, parent,false);
            viewHolder = new ViewHolder();
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            viewHolder.imLogo=(ImageView)convertView.findViewById(R.id.ivLogo);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvTitle.setText(file.getName());
//        Drawable drawable = parent.getContext().getResources().getDrawable(R.drawable.tovar_defoult_ico_small);
        Picasso.with(parent.getContext()).load(file).placeholder(R.drawable.tovar_defoult_ico_small).fit().into(viewHolder.imLogo);
        return convertView;
    }

    static class ViewHolder {
        private TextView tvTitle;
        private ImageView imLogo;
    }
}
