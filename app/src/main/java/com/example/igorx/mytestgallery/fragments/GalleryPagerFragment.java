package com.example.igorx.mytestgallery.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.igorx.mytestgallery.R;
import com.example.igorx.mytestgallery.ZoomOutPageTransformer;
import com.example.igorx.mytestgallery.adapters.GalleryItemPagerAdapter;

public class GalleryPagerFragment extends Fragment  {

    private ViewPager viewPager;
    private GalleryItemPagerAdapter adapter;
    private static final String SELECT_POSITION= "POSITION";
    private int mPosition;
    private FabListener fabListener;

    public static Fragment newInstance(int position) {
        Fragment fragment = new GalleryPagerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(SELECT_POSITION, position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fabListener= (FabListener) context;
    }

    @Override
    public void onResume() {
        super.onResume();
        fabListener.showFab();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mPosition=getArguments().getInt(SELECT_POSITION);
        View view = inflater.inflate(R.layout.gallery_pager_fragment, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        adapter = new GalleryItemPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(mPosition);
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        return view;
    }
    @Override
    public void onPause() {
        super.onPause();
        fabListener.hideFab();
    }

    public interface FabListener {
        void showFab();

        void hideFab();
    }
}
